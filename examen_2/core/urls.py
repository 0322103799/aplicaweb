from django.urls import path
from core import views



app_name = "core"


urlpatterns = [
    path('list/grantgoal/', views.ListGrantGoal.as_view(), name="gg_list"),
    path('create/grantgoal/', views.CreateGrantGoal.as_view(), name="gg_create"),
    path('detail/grantgoal/<int:pk>/', views.DetailGrantGoal.as_view(), name="gg_detail"),
    path('update/grantgoal/<int:pk>/', views.UpdateGrantGoal.as_view(), name="gg_update"),
    path('delete/grantgoal/<int:pk>/', views.DeleteGrantGoal.as_view(), name="gg_delete"),
    path('list/partidos/', views.ListaPartidos.as_view(), name="p_list"),
    path('create/partidos/', views.CreatePartidos.as_view(), name="p_create"),
    path('detail/partidos/<int:pk>/', views.DetailPartidos.as_view(), name="p_detail"),
    path('update/partidos/<int:pk>/', views.UpdatePartidos.as_view(), name="p_update"),
    path('delete/partidos/<int:pk>/', views.DeletePartidos.as_view(), name="p_delete"),
    path('list/equipos/', views.ListaEquipos.as_view(), name="e_list"),
    path('create/equipos/', views.CreateEquipos.as_view(), name="e_create"),
    path('detail/equipos/<int:pk>/', views.DetailEquipos.as_view(), name="e_detail"),
    path('update/equipos/<int:pk>/', views.UpdateEquipos.as_view(), name="e_update"),
    path('delete/equipos/<int:pk>/', views.DeleteEquipos.as_view(), name="e_delete"),
    path('list/estadios/', views.ListaEstadios.as_view(), name="es_list"),
    path('create/estadios/', views.CreateEstadios.as_view(), name="es_create"),
    path('detail/estadios/<int:pk>/', views.DetailEstadios.as_view(), name="es_detail"),
    path('update/estadios/<int:pk>/', views.UpdateEstadios.as_view(), name="es_update"),
    path('delete/estadios/<int:pk>/', views.DeleteEstadios.as_view(), name="es_delete"),
    path('list/ciudades/', views.ListaCiudades.as_view(), name="c_list"),
    path('create/ciudades/', views.CreateCiudades.as_view(), name="c_create"),
    path('detail/ciudades/<int:pk>/', views.DetailCiudades.as_view(), name="c_detail"),
    path('update/ciudades/<int:pk>/', views.UpdateCiudades.as_view(), name="c_update"),
    path('delete/ciudades/<int:pk>/', views.DeleteCiudades.as_view(), name="c_delete"),
]