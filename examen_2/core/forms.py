


from django import forms
from .models import GrantGoal
from .models import Partidos
from .models import Equipos
from .models import Estadios
from .models import Ciudades

class GrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = "__all__"
        exclude = ["timestamp", "final_date"]
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "gg_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del GrantGoal"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"Escribe la descripcion del GrantGoal"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "state": forms.Select(attrs={"type":"select","class":"form-select"}),
            "sprint":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del GrantGoal"}),
        }



class UpdateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = "__all__"
        exclude = ["timestamp", "final_date"]
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "gg_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del GrantGoal"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"Escribe la descripcion del GrantGoal"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "state": forms.Select(attrs={"type":"select","class":"form-select"}),
            "sprint":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del GrantGoal"}),
        }

### Partidos ###
class PartidosForms(forms.ModelForm):
    class Meta:
        model = Partidos
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "local": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "visitante": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "estadio": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del estadio"}),
            "fecha": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la fecha del partido"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "estado":forms.Select(attrs={"type":"select","class":"form-select"}),
            "duracion":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }

class UpgradePartidosForms(forms.ModelForm):
    class Meta:
        model = Partidos
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "local": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "visitante": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "estadio": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del estadio"}),
            "fecha": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la fecha del partido"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "estado":forms.Select(attrs={"type":"select","class":"form-select"}),
            "duracion":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }

### Equipos ###
class EquiposForms(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "equipo": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "ciudad": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "ganados":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "liga": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la fecha del partido"}),
            "perdidos":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "estado":forms.Select(attrs={"type":"select","class":"form-select"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }

class UpgradeEquiposForms(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "equipo": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "ciudad": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "ganados":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "liga": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la fecha del partido"}),
            "perdidos":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "estado":forms.Select(attrs={"type":"select","class":"form-select"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }

### Estadios ###
class EstadiosForms(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "estadio": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "ciudad": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "equipos": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del estadio"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }

class UpgradeEstadiosForms(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "estadio": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "ciudad": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "estadio": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del estadio"}),
            "equipos": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la fecha del partido"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }

### Ciudades ###
class CiudadesForms(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "ciudad": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "equipos": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }

class UpgradeCiudadesForms(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "ciudad": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo local"}),
            "equipos": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del equipo visitante"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del partido"}),
        }