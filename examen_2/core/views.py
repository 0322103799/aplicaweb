from django.shortcuts import render

from django.views import generic

from django.urls import reverse_lazy
from .models import GrantGoal
from .models import Partidos
from .models import Equipos
from .models import Estadios
from .models import Ciudades
from .forms import GrantGoalForm, UpdateGrantGoalForm, PartidosForms, UpgradePartidosForms, EquiposForms, UpgradeEquiposForms, EstadiosForms, UpgradeEstadiosForms, CiudadesForms, UpgradeCiudadesForms
# Create your views here.

##### G R A N T G O A L C R U D #####

##Create
class CreateGrantGoal(generic.CreateView):
    template_name = "core/gg_create.html"
    model = GrantGoal
    form_class = GrantGoalForm
    success_url = reverse_lazy("core:gg_list")


## Retrieve
#List GrantGoal
class ListGrantGoal(generic.View):
    template_name = "core/gg_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = GrantGoal.objects.filter(status=True)
        self.context = {
            "grant_goals": queryset
        }
        return render(request, self.template_name, self.context)


#Detail
class DetailGrantGoal(generic.DetailView):
    template_name = "core/gg_detail.html"
    model = GrantGoal


#Update
class UpdateGrantGoal(generic.UpdateView):
    template_name = "core/gg_update.html"
    model = GrantGoal
    form_class = UpdateGrantGoalForm
    success_url = reverse_lazy("core:gg_list")


#Delete
class DeleteGrantGoal(generic.DeleteView):
    template_name = "core/gg_delete.html"
    model = GrantGoal
    success_url = reverse_lazy("core:gg_list")

###-----------------partidos-----------------###
### list ###
class ListaPartidos(generic.View):
    template_name = "core/p_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Partidos.objects.filter(status=True)
        self.context = {
            "partidos": queryset
        }
        return render(request, self.template_name, self.context)
    
### create ###
class CreatePartidos(generic.CreateView):
    template_name = "core/p_create.html"
    model = Partidos
    form_class = PartidosForms
    success_url = reverse_lazy("core:p_list")

### detail ###
class DetailPartidos(generic.DetailView):
    template_name = "core/p_detail.html"
    model = Partidos

### update ###
class UpdatePartidos(generic.UpdateView):
    template_name = "core/p_update.html"
    model = Partidos
    form_class = UpgradePartidosForms
    success_url = reverse_lazy("core:p_list")

### delete ###
class DeletePartidos(generic.DeleteView):
    template_name = "core/p_delete.html"
    model = Partidos
    success_url = reverse_lazy("core:p_list")

###-----------------Equipos-----------------###
### list ###
class ListaEquipos(generic.View):
    template_name = "core/e_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Equipos.objects.filter(status=True)
        self.context = {
            "equipos": queryset
        }
        return render(request, self.template_name, self.context)
    
### create ###
class CreateEquipos(generic.CreateView):
    template_name = "core/e_create.html"
    model = Equipos
    form_class = EquiposForms
    success_url = reverse_lazy("core:e_list")

### detail ###
class DetailEquipos(generic.DetailView):
    template_name = "core/e_detail.html"
    model = Equipos

### update ###
class UpdateEquipos(generic.UpdateView):
    template_name = "core/e_update.html"
    model = Equipos
    form_class = UpgradeEquiposForms
    success_url = reverse_lazy("core:e_list")

### delete ###
class DeleteEquipos(generic.DeleteView):
    template_name = "core/e_delete.html"
    model = Equipos
    success_url = reverse_lazy("core:e_list")

###-----------------Estadios-----------------###
### list ###
class ListaEstadios(generic.View):
    template_name = "core/es_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Estadios.objects.filter(status=True)
        self.context = {
            "estadios": queryset
        }
        return render(request, self.template_name, self.context)
    
### create ###
class CreateEstadios(generic.CreateView):
    template_name = "core/es_create.html"
    model = Estadios
    form_class = EstadiosForms
    success_url = reverse_lazy("core:es_list")

### detail ###
class DetailEstadios(generic.DetailView):
    template_name = "core/es_detail.html"
    model = Estadios

### update ###
class UpdateEstadios(generic.UpdateView):
    template_name = "core/es_update.html"
    model = Estadios
    form_class = UpgradeEstadiosForms
    success_url = reverse_lazy("core:es_list")

### delete ###
class DeleteEstadios(generic.DeleteView):
    template_name = "core/es_delete.html"
    model = Estadios
    success_url = reverse_lazy("core:es_list")

###-----------------Ciudades-----------------###
### list ###
class ListaCiudades(generic.View):
    template_name = "core/c_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Estadios.objects.filter(status=True)
        self.context = {
            "ciudades": queryset
        }
        return render(request, self.template_name, self.context)
    
### create ###
class CreateCiudades(generic.CreateView):
    template_name = "core/c_create.html"
    model = Ciudades
    form_class = CiudadesForms
    success_url = reverse_lazy("core:c_list")

### detail ###
class DetailCiudades(generic.DetailView):
    template_name = "core/c_detail.html"
    model = Ciudades

### update ###
class UpdateCiudades(generic.UpdateView):
    template_name = "core/c_update.html"
    model = Ciudades
    form_class = UpgradeCiudadesForms
    success_url = reverse_lazy("core:c_list")

### delete ###
class DeleteCiudades(generic.DeleteView):
    template_name = "core/c_delete.html"
    model = Ciudades
    success_url = reverse_lazy("core:c_list")