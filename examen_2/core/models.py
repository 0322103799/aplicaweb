from django.db import models

from django.contrib.auth.models import User

from datetime import timedelta
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

STATE_CHOICES = (
    ('Done', 'DN'),
    ('Doing', 'DG'),
    ('Not Stared', 'NS'),
)

ESTADO_EQUIPOS = (
    ('Activo', 'AC'),
    ('Fuera', 'FU'),
)

ESTADO_PARTIDOS = (
    ('En espera', 'ES'),
    ('Dia del partido', 'DP'),
    ('Finalizado', 'FN'),
)

class GrantGoal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    gg_title = models.CharField(max_length=128, default="Generic Grant Goal Title")
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    timestamp = models.DateField(auto_now_add=True)
    days_duration = models.IntegerField(default=7)
    final_date = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=STATE_CHOICES)
    sprint = models.IntegerField(default=1)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.gg_title



# ## SIGNALS 
# ### post_save
@receiver(post_save, sender=GrantGoal)
def end_time_grantgoal(sender, instance, **kwargs):
    if instance.final_date is None or instance.final_date=='':
        instance.final_date = instance.timestamp + timedelta(days=instance.days_duration)
        instance.save()

### Partidos ###
class Partidos(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    local = models.CharField(max_length=128, default="Local")
    visitante = models.CharField(max_length=256, default="Visitante")
    estadio = models.CharField(max_length=256, default="Estadio")
    fecha = models.CharField(max_length=128, default="Fecha")
    status = models.BooleanField(default=True)
    estado = models.CharField(max_length=16, choices=ESTADO_PARTIDOS)
    duracion = models.IntegerField(default=90)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.local
    
### equipos ###
class Equipos(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    equipo = models.CharField(max_length=128)
    ciudad = models.CharField(max_length=256)
    ganados = models.IntegerField(default=0)
    liga = models.CharField(max_length=256)
    perdidos = models.IntegerField(default=0)
    status = models.BooleanField(default=True)
    estado = models.CharField(max_length=16, choices=ESTADO_EQUIPOS)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.equipo

### estadios ###
class Estadios(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    estadio = models.CharField(max_length=128)
    ciudad = models.CharField(max_length=256)
    equipos = models.IntegerField(default=0)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.estadio
    
### ciudades ###
class Ciudades(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ciudad = models.CharField(max_length=128)
    equipos = models.CharField(max_length=256)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.ciudad