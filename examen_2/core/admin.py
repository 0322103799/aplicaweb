from django.contrib import admin

# Register your models here.
from .models import GrantGoal
from .models import Partidos
from .models import Equipos
from .models import Estadios
from .models import Ciudades

@admin.register(GrantGoal)
class GrantGoalAdmin(admin.ModelAdmin):
    list_display = [
        "gg_title",
        "timestamp",
        "days_duration",
        "final_date",
        "user"
    ]

@admin.register(Partidos)
class PartidosAdmin(admin.ModelAdmin):
    list_display = [
        "local",
        "visitante",
        "estadio",
        "fecha",
        "estado",
        "user"
    ]

@admin.register(Equipos)
class EquiposAdmin(admin.ModelAdmin):
    list_display = [
        "equipo",
        "ciudad",
        "ganados",
        "liga",
        "perdidos",
        "user"
    ]

@admin.register(Estadios)
class EstadiosAdmin(admin.ModelAdmin):
    list_display = [
        "estadio",
        "ciudad",
        "equipos",
        "user"
    ]

@admin.register(Ciudades)
class CiudadesAdmin(admin.ModelAdmin):
    list_display = [
        "ciudad",
        "equipos",
        "user"
    ]