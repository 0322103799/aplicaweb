from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from .forms import PuestoForm, EmpleadoForm, AlergiaForm
from .models import Puesto, Empleado, Alergia


#Puesto
class CreatePuesto(CreateView):
    template_name = "core/create_puesto.html"
    model = Puesto
    form_class = PuestoForm
    success_url = reverse_lazy('core:list_puesto')

class ListPuesto(ListView):
    template_name = "core/list_puesto.html"  
    model = Puesto
    context_object_name = 'Puestos'

class DetailPuesto(DetailView):
    template_name = "core/detail_puesto.html"
    model = Puesto
    context_object_name = 'Puesto'

class UpdatePuesto(UpdateView):
    template_name = "core/update_puesto.html"
    model = Puesto
    form_class = PuestoForm
    success_url = reverse_lazy('core:list_puesto')

class DeletePuesto(DeleteView):
    template_name = "core/delete_puesto.html"
    model = Puesto
    success_url = reverse_lazy('core:list_puesto')
    
    #Empleado
class CreateEmpleado(CreateView):
    template_name = "core/create_empleado.html"
    model = Empleado
    form_class = EmpleadoForm
    success_url = reverse_lazy('core:list_empleado')

class ListEmpleado(ListView):
    template_name = "core/list_empleado.html"  
    model = Empleado
    context_object_name = 'Empleados'

class DetailEmpleado(DetailView):
    template_name = "core/detail_empleado.html"
    model = Empleado
    context_object_name = 'Empleado'

class UpdateEmpleado(UpdateView):
    template_name = "core/update_empleado.html"
    model = Empleado
    form_class = EmpleadoForm
    success_url = reverse_lazy('core:list_empleado')

class DeleteEmpleado(DeleteView):
    template_name = "core/delete_empleado.html"
    model = Empleado
    success_url = reverse_lazy('core:list_empleado')
    
    #Alergia
class CreateAlergia(CreateView):
    template_name = "core/create_alergia.html"
    model = Alergia
    form_class = AlergiaForm
    success_url = reverse_lazy('core:list_alergia')

class ListAlergia(ListView):
    template_name = "core/list_alergia.html"  
    model = Alergia
    context_object_name = 'Alergias'

class DetailAlergia(DetailView):
    template_name = "core/detail_alergia.html"
    model = Alergia
    context_object_name = 'Alergia'

class UpdateAlergia(UpdateView):
    template_name = "core/update_alergia.html"
    model = Alergia
    form_class = AlergiaForm
    success_url = reverse_lazy('core:list_alergia')

class DeleteAlergia(DeleteView):
    template_name = "core/delete_alergia.html"
    model = Alergia
    success_url = reverse_lazy('core:list_alergia')